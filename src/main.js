// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueResource from 'vue-resource'
// import router from './router'
import VueRouter from 'vue-router'
import goods from './components/goods/goods'
import seller from './components/seller/seller.vue'
import ratings from './components/ratings/ratings.vue'

import './common/stylus/index.styl'

const routes = [
  {path: '/goods', component: goods},
  {path: '/seller', component: seller},
  {path: '/ratings', component: ratings},
  {path: '/', redirect: '/goods'}
]
const router = new VueRouter({
  routes,
  linkActiveClass: 'active'
})

Vue.use(VueRouter)
Vue.use(VueResource)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  // router,
  router,
  template: '<App/>',
  components: { App }
}).$mount('#app')
